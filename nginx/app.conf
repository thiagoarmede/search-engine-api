# Configure the reverse-proxy on port 443
http {
  # general configs
  keepalive_timeout    30;
  listen               127.0.0.1:443 ssl;
  server_name          api.example.com;

  # ssl configs
  ssl_certificate      /path/to/api.crt;
  ssl_certificate_key  /path/to/api.key;
  ssl_session_cache    shared:SSL:10m;
  ssl_session_timeout  10m;

  # proxy to the nodejs applciation
  location / {

    # handle OPTIONS requests
    # @note: don't try to DRY out this "if" block, or you're gonna have a bad time.
    # @see: http://wiki.nginx.org/IfIsEvil
    if ($request_method = 'OPTIONS') {
      add_header 'Access-Control-Allow-Credentials' 'true';
      add_header 'Access-Control-Allow-Headers'     'Authorization,Content-Type,Accept,Origin,User-Agent,DNT,Cache-Control,X-Mx-ReqToken,Keep-Alive,X-Requested-With,If-Modified-Since';
      add_header 'Access-Control-Allow-Methods'     'GET, DELETE, OPTIONS, POST, PUT';
      add_header 'Access-Control-Allow-Origin'      '*';
      add_header 'Access-Control-Max-Age'           2592000;
      add_header 'Content-Length'                   0;
      add_header 'Content-Type'                     'text/plain charset=UTF-8';
      return 204;
    }

    # proxy to the nodejs application
    client_max_body_size 64G;
    proxy_pass   http://127.0.0.1:8080;

    # send the CORS headers
    add_header 'Access-Control-Allow-Credentials' 'true';
    add_header 'Access-Control-Allow-Origin'      '*';

    # set additional security headers
    add_header 'Cache-Control'                    'no-cache, no-store, must-revalidate';
    add_header 'Content-Security-Policy'          'connect-src example.com';
    add_header 'Expires'                          '0';
    add_header 'Pragma'                           'no-cache';
    add_header 'Strict-Transport-Security'        'max-age=31536000; includeSubDomains';
    add_header 'X-Content-Type-Options'           'nosniff';
    add_header 'X-Frame-Options'                  'DENY';
    add_header 'X-XSS-Protection'                 '1; mode=block';
  }

  # logs paths
  access_log /path/to/access.log;
  error_log  /path/to/error.log;
}

# Rewrite all trafic on port 80 to 443
server {
  listen         127.0.0.1:80;
  server_name    api.example.com;
  rewrite        ^ https://$server_name:8080$request_uri? permanent;
}