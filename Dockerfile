FROM node:8
# Create app directory
WORKDIR /usr/src/app
COPY package*.json ./

RUN npm install
RUN npm install ts-node -g
RUN npm install typescript -g
RUN npm install jest -g

EXPOSE 8080

COPY run.sh /src/run.sh
COPY . .

CMD [ "/bin/sh", "/src/run.sh" ]