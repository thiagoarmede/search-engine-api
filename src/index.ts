import restify from "restify";
import elastic from "elasticsearch";
import { Router } from "./routes/Router";
import { Engine } from "./lib/Search/Engine";
import corsMiddleware from "restify-cors-middleware";

const server = restify.createServer();

const elasticClient = new elastic.Client({
    host: "http://elasticsearch:9200",
    log: "trace"
});

const cors = corsMiddleware({
    origins: ["*"],
    allowHeaders: ["API-Token", "Access-Control-Allow-Origin"],
    exposeHeaders: ["API-Token-Expiry"]
});

elasticClient.ping(
    {
        requestTimeout: 3000
    },
    error => {
        if (error) {
            console.trace("Elasticsearch cluster is down!");
        } else {
            console.log("Elasticsearch cluster ok.");
        }
    }
);

const engine = new Engine(elasticClient);

server.pre(cors.preflight);
server.use(restify.plugins.bodyParser());
server.use(restify.plugins.queryParser());
server.use(cors.actual);

Router.setRoutes(server, engine);

server.listen(8080, () => {
    console.log(`Server listening at ${server.url}`);
});
