import restify from "restify";
import { Search } from "../lib/Search/SearchController";
import { Engine } from "../lib/Search/Engine";

export class Router {
    public static setRoutes(server: restify.Server, engine: Engine) {
        server.get("/entities", Search.queryData(engine));
        server.post("/entities", Search.insertData(engine));
    }
}
