import elastic from "elasticsearch";
import { errorTap } from "../../helpers/utils";

type IDocument = {
    type: string;
    [key: string]: any;
};

export class Engine {
    private client: elastic.Client;

    constructor(client: elastic.Client) {
        this.client = client;
    }

    private async createIndex(index: string) {
        await this.client.indices
            .create({
                index
            })
            .catch(errorTap);
    }

    public async indexDocument(doc: IDocument) {
        const { type, ...data } = doc;
        const index = doc.type.toLowerCase();

        await this.client
            .index({
                index,
                type,
                body: {
                    ...data
                }
            })
            .catch(errorTap);
    }

    public async verifyAndindexDocument(
        doc: IDocument
    ): Promise<boolean | Error> {
        try {
            const index = doc.type.toLowerCase();
            const { type, ...data } = doc;
            await this.indexDocument(doc).catch(errorTap);

        } catch (err) {
            return err;
        }

        return true;
    }

    public async searchDocument(alikeData: string, filter?: string) {
        const searchResults = await this.client
            .search({
                index: filter ? filter.toLowerCase() : undefined,
                body: {
                    from: 0,
                    query: {
                        match: {
                            title: {
                                query: alikeData,
                                fuzziness: 5
                            }
                        }
                    }
                }
            })
            .catch(errorTap);
        
        const formattedResult = (searchResults as elastic.SearchResponse<{}> ).hits!.hits.map(h => h._source);

        return formattedResult;
    }
}
