import { Request, Response, Next } from "restify";
import { Engine } from "./Engine";

export class Search {
    public static queryData = (engine: Engine) => async (
        req: Request,
        res: Response,
        next: Next
    ) => {
        try {
            if (req.query) {
                const dataToReturn = await engine
                    .searchDocument(
                        req.query.q,
                        req.query.entity_type || undefined
                    );
                
                res.send(200, dataToReturn);
            } else {
                res.send(500, new Error("Incorrect params"));
            }
        } catch (e) {
            res.send({ error: e.message });
        }
        next();
    };

    public static insertData = (engine: Engine) => async (
        req: Request,
        res: Response,
        next: Next
    ) => {
        const parsedData =
            typeof req.body === "string" ? JSON.parse(req.body) : req.body;
        const result = await engine
            .indexDocument(parsedData)
            .catch(console.log);
        res.send(200, result);
        next();
    };
}
