import { errorTap } from "./utils";

describe("Tests on utility functions.", () => {
    test("should run a function and throw an error", () => {
        const fn = jest.fn(e => console.log(e));
        const e = "testing function";
        expect(() => errorTap(fn)(e)).toThrow(e);
    });
});
