export const errorTap = (fn: (err: any) => void) => (e: any) => {
    fn(e);
    throw e;
};
