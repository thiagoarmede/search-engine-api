import restify from "restify";
import elasticsearch from "elasticsearch";
import { Router } from "./routes/Router";
import { Engine } from "./lib/Search/Engine";
import request from "supertest";

const server = restify.createServer();

const elasticClient = new elasticsearch.Client({
    host: "localhost:9200"
});

elasticClient.ping(
    {
        requestTimeout: 3000
    },
    function(error) {
        if (error) {
            console.trace("Elasticsearch cluster is down!");
        } else {
            console.log("Elasticsearch cluster ok.");
        }
    }
);

const engine = new Engine(elasticClient);

Router.setRoutes(server, engine);

server.use(restify.plugins.bodyParser());
server.use(restify.plugins.queryParser());

server.listen(8080, () => {
    console.log(`Server listening at ${server.url}`);
});

describe("Simple integration testing", () => {
    test("Should insert data on 'topic' index.", done => {
        request(server)
            .post("/entities")
            .set("Accept", "application/json")
            .send({ title: "Some Title", type: "TOPIC" })
            .expect(200)
            .end((err, _) => {
                if (err) {
                    return done(err);
                }
                done();
            });
    });

    test("Should insert data on 'person' index.", done => {
        request(server)
            .post("/entities")
            .set("Accept", "application/json")
            .send({ title: "Some Person", type: "PERSON" })
            .expect(200)
            .end((err, _) => {
                if (err) {
                    return done(err);
                }
                done();
            });
    });

    test("Should query some data.", done => {
        request(server)
            .get("/entities?q=some")
            .expect(200)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                if (res) console.log(res.body.hits);
                done();
            });
    });

    afterAll(async () => {
        console.log("called afterAll");

        elasticClient.close();
    });
});
